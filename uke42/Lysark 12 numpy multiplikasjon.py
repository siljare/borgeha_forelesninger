import numpy as np
a = np.array([[1,2,3,4,5],[3,4,5,6,7],[2,2,3,4,8]])
b = np.array([[2,4,6],[6,8,10],[1,2,3],[2,3,4],[44,3,2]])
af = a.shape
bf = b.shape
print("shapes:",af,bf)
print("af[0]",af[0])
print(f'Matrise a:\n{a}')
print(f'Matrise b:\n{b}')
print(f'\nFor at matrisene skal kunne multipliseres må de ha passende form.')
print(f'af * bf: {af} * {bf}')
if af[1] == bf[0]:
    print(f'\na og b multiplisert blir \n{a@b}\n')
else:
    print('Matrisene kan ikke multipliseres')
