# Nok en gang: Se igjennom foilene, for de fleste av dem
# ligger koden på git. Takk Terje. ;-)
# Jeg rekker ikke å gå igjennom alle muligheter!

# En streng består av tegn, som kan hentes
# ut som elementer i en liste:

streng = "Kapang!"
print("Første tegn:", streng[0])

# Hva skjer hvis du skriver ut streng[len(streng)]?
# print(streng[len(streng)])


# Den kan også slices akkurat som lister:
print("Tre første tegn, baklengs:",streng[2::-1])

# Vi kan gå gjennom tegnene i for-løkker:
for tegn in streng:
    print(tegn)

# Vi kan bruke løkke med indeks:
for i in range(len(streng)):
    print(streng[i])

# Vi kan samkjøre det med list comprehension:
print([c for c in streng])

# Oppgave: tall forekomster av 'a' i en streng: oppgave_tell_a

# Strenger kan settes sammen:
a = "hei"
b = " på deg"
a += b # Det til høyre for += må finnes allerede!
print(a)

# Merk at strenger er ikke-muterbare. a blir en helt ny streng
# på en ny plass i minnet. Ikke som lister!

# Hvis vi skal sette inn tall må i gjøre det om til en streng:
tall = 42
streng = "Svaret er: "
streng += str(42)

print(streng) # f strings gjør det litt enklere:
streng = "Svaret er: "
print(f'{streng}{tall}')

# Oppgave: Cæsar-rotasjon: oppgave_caesar_rotasjon

# Jeg vil legge inn flere tall på en gang, hvordan gjør man slikt!
streng = "2 4 44 6 43 19"

# få dem inn en liste:
liste = streng.split()
print(liste)
# Men vi må gjøre dem om til tall!
liste2 = [int(i) for i in streng.split()]
print(liste2)
print(list(map(int, streng.split()))) # Denne trenger dere ikke pugge, altså!

# Man kan splitte på hva som helst.
print("trudderudderantan".split('ru'))  # Merk at splittestrengen ikke blir med


# Søk i strenger
streng = 'i høstferien stod jeg i tolv timer og brant bål- Hosthost'
print(f"'høst' in streng: {'høst' in streng}")
print("Første 'høst' starter på plass", streng.index('høst')) # Startplassen til første substreng 'høst'

# Strenger har mange operasjoner man kan kjøre på dem: se lysark39.py
# Se også lysark42.py

# Vi kan kjøre replace!
streng = "Dette var en setning som var litt for kor"
streng = streng.replace('var','er')
print(streng) # replace returnerer en ny streng med alle erstattet!

