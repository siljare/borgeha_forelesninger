import numpy as np

def fyllArray(matrise):
    for i in range(matrise.shape[0]):
        for j in range(matrise.shape[1]):
            matrise[i,j] = int(input(f'Oppgi verdi for koordinat [{i},{j}]]: '))

rader = int(input('Hvor mange rader: '))
kolonner = int(input('Hvor mange kolonner: '))

arr = np.zeros((rader,kolonner))
print(arr)

fyllArray(arr)
print(arr)