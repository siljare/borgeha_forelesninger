streng = '\tDette er en streng.  \t'
nystreng = 'Dette er en streng. D'


streng1 = streng.lower()             # Endrer til små bokstaver.
streng2 = streng.lstrip()            # Fjerner whitespace i begynnelsen.
streng3 = nystreng.lstrip('D')       # Fjerner D-en først i setningen
streng4 = streng.rstrip()            # Fjerner whitespace på slutten.
streng5 = nystreng.rstrip('. D')     # Fjerner ". D" sist i setningen
streng6 = streng.strip()             # Fjerner whitespace i start og slutt.
streng7 = nystreng.strip('D')        # Fjerner "D" i start og slutt.
streng8 = streng.upper()             # Endrer til store bokstaver.

print('1:',streng1,'Endret til små bokstaver.')
print('2:',streng2,'\tFjernet whitespace i begynnelsen.')
print('3:',streng3,'\t\tFjernet D-en først i setningen')
print('4:',streng4,'\tFjernet whitespace på slutten.')
print('5:',streng5,'\t\tFjernet ". D" sist i setningen')
print('6:',streng6,'\t\tFjernet whitespace i start og slutt.')
print('7:',streng7,'\t\tFjernet "D" i start og slutt.')
print('8:',streng8,'Endret til store bokstaver.')